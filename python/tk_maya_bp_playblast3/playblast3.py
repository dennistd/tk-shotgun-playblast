import os
import site
import json
import logging
from pprint import pprint, pformat

FORMAT = '%(asctime)-15s:: %(filename)s:%(lineno)s => %(funcName)s(): %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)

from functools import partial
import traceback

import maya.OpenMayaUI as OmUI
import pymel.core as pm
import maya.mel as mel

import thirdparty.yaml as yaml

from tank.platform.qt import QtCore, QtGui
import previz_hud as hud

from .playblast3_gui import PlayblastTool3GUI

reload(hud)

# In house tool site
tool_site = os.path.join("U:", "tools")
site.addsitedir(tool_site)

ui_file = os.path.join(os.path.dirname(__file__), 'playblast3.ui')
this_dir = os.path.dirname(__file__)
json_config_file = os.path.join(this_dir, 'studio_configurations.json')
yaml_preset_file = os.path.join(this_dir, 'studio_presets.yml')


class PlayblastTool3(object):
    def __init__(self, app, context=None):
        """
        class init method. Set parent to Maya Main Window if no parent is given
        :param parent:
        :return:
        """

        #
        # Shotgun App initialization
        #
        self._app = app
        self._context = context if context else self._app.context

        #
        # Main Code
        #

        # set HUD to false
        self.hud_state = False

        self.shot_playblast_path = None
        self.temp_output_path = None
        self.__upload_to_shotgun = True
        self.__use_custom_hud = True

    def show_ui(self):
        try:
            self._app.engine.show_dialog("BP Playblast3 (SG) %s" % self._app.version,
                                         self._app, PlayblastTool3GUI, self._app, self)
        except StandardError as e:
            traceback.print_exc()
            print e

    def get_temp_dir(self, output_path):
        # Get value of optional config field "temp_directory". If path is
        # invalid or not absolute, use default tempdir.
        temp_directory = os.path.normpath(self._app.get_setting("temp_directory", "default"))
        if not os.path.isabs(temp_directory):
            import tempfile
            temp_directory = tempfile.gettempdir()

        # make sure it is exists
        if not os.path.isdir(temp_directory):
            os.mkdir(temp_directory)

        # use the basename of generated names
        temp_output_path = os.path.join(temp_directory, os.path.basename(output_path))

        return temp_output_path

    def do_playblast(self, job_flag_dict=None):
        """
        Init playblast environment and context from sg toolkit

        """

        if not isinstance(job_flag_dict, dict):
            pm.confirmDialog(title="Cannot get valid job flags",
                             message="Make sure gui return valid job flag data")
            return

        template_work = self._app.get_template("template_work")
        template_shot = self._app.get_template("template_shot")

        scene_name = pm.sceneName()

        if not scene_name or len(scene_name) <= 1:
            pm.confirmDialog(title="File Not Saved!!",
                             message="Please save the scene file with 'Shotgun Save As' first")
            return

        fields = template_work.get_fields(scene_name)

        # use local temp path for playblast output
        self.shot_playblast_path = template_shot.apply_fields(fields)
        self.temp_output_path = self.get_temp_dir(output_path=self.shot_playblast_path)

        self.do_single_playblast(job_data=job_flag_dict, output_path=self.temp_output_path)

    def get_playblast_dest_path(self):
        return self.shot_playblast_path

    def do_single_playblast(self, job_data=None, output_path=None):
        """
        Single Playblast Mode
        :param output_dir:
        :return:
        """

        job_flag_dict = job_data

        if not isinstance(job_flag_dict, dict):
            logger.debug("{} not a dict".format(job_flag_dict))

        current_huds = None

        # call pre playblast hooks
        if self.__use_custom_hud:
            current_huds = self._app.execute_hook("hook_pre_playblast", action="setup_hud")

        # update output file path to job flag data

        job_flag_dict = self._app.execute_hook("hook_pre_playblast", action="set_output_path",
                                               data=job_flag_dict,
                                               output_path=output_path)

        logger.info(pformat(job_flag_dict))

        # actually do the playblast, sigh
        result = do_playblast_output_job(**job_flag_dict)

        if not result:
            self._app.log_debug("Something wrong with Playblast")
            return

        # restore hud after the fact
        self._app.execute_hook("hook_post_playblast", action="restore_hud", data=current_huds)

        if os.path.isfile(output_path):
            self._app.log_info("{} is prime for copy and uplaod".format(output_path))

        else:
            self._app.log_debug("{} is missing".format(output_path))
            return

        # do post playblast process, copy files and other necessary stuff

        result = self._app.execute_hook("hook_post_playblast", action="copy_file", data=output_path)

        if result:
            self._app.log_info("Playblast local file created: %s" % output_path)
        else:
            self._app.log_debug("Failed to create local file: {}".format(output_path))
            return

        # register new Version entity in shotgun or update existing version, minimize shotgun data
        playblast_movie = self.get_playblast_dest_path()
        project = self._app.context.project
        entity = self._app.context.entity
        task = self._app.context.task

        data = {'project': project,
                'code': os.path.basename(playblast_movie),
                'description': 'Generated by BP-Playblast3 app',
                'sg_path_to_movie': playblast_movie,
                'entity': entity,
                'sg_task': task,
                }

        self._app.log_debug("Version-creation hook data:\n" + pformat(data))
        result = self._app.execute_hook("hook_post_playblast", action="create_version", data=data)
        self._app.log_debug("Version-creation hook result:\n" + pformat(result))

        # upload QT file if creation or update process run succesfully
        if result and self.__upload_to_shotgun:
            result = self._app.execute_hook("hook_post_playblast", action="upload_movie",
                                            data=dict(path=data["sg_path_to_movie"],
                                                      project=project,
                                                      version_id=result["id"]))

        self._app.log_info("Playblast finished")


    def set_upload_to_shotgun(self, value):
        """
        Set whether or not to upload movie to SG

        """
        self._app.log_debug("Upload to Shotgun set to %s" % value)
        self.__upload_to_shotgun = value

    def get_upload_to_shotgun(self):
        return self.__upload_to_shotgun

    def set_use_custom_hud(self, val):
        self._app.log_debug("Use Custom HUD is set to {}".format(val))
        self.__use_custom_hud = val

    def get_use_custom_hud(self):
        return self.__use_custom_hud


def return_regex_compiled(input_str=None):
    """
    Return python regex object from regex string

    """
    if not input_str or not isinstance(input_str, str):
        return None

    import re

    return re.compile(input_str)


def do_playblast_output_job(**job_flag_dict):
    """
    Take a constrcted job flag dictionary and use that as basis to launch maya playblast

    """

    if not job_flag_dict:
        logger.debug("Job Flags data invalid, please check the job flag contsruction")
        return

    # Handle some Maya general exception related to playblast
    result = handle_maya_general_exceptions(job_dict=job_flag_dict)

    try:
        if result["HAS_STEREO_SETUP"]:
            if not result["STEREO_CAM_EXISTS"]:
                logger.info("Select Stereo Camera option but scene has no Stereo Cam, Skip ... \n")
                return

    except StandardError as e:
        print e
        return

    try:
        pm.playblast(**job_flag_dict)
        return True
    except StandardError as e:
        print e


def handle_maya_general_exceptions(job_dict=None):
    result = {"HAS_STEREO_SETUP": False, "STEREO_CAM_EXISTS": False, "ACTIVE_STEREO": False}

    if job_dict and isinstance(job_dict, dict):

        if 'cameraSetup' in job_dict:
            for item in job_dict['cameraSetup']:
                if 'stereo' in item[0]:
                    result['HAS_STEREO_SETUP'] = True

                    if len(pm.ls(type='stereoRigCamera')) == 0:
                        result["STEREO_CAM_EXISTS"] = False
                    else:
                        result["STEREO_CAM_EXISTS"] = True
                    return result

                else:
                    return result

    return result
