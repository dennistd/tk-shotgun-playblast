import os
import site
import json
import logging

from functools import partial
from pprint import pformat

import maya.OpenMayaUI as OmUI
import pymel.core as pm
import maya.mel as mel

import thirdparty.yaml as yaml

from PySide import QtGui
import pyside_dynamic as pys

import previz_hud as hud

FORMAT = '%(asctime)-15s:: %(filename)s:%(lineno)s => %(funcName)s(): %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)

reload(hud)

tool_site = os.path.join("U:", "tools")
site.addsitedir(tool_site)

ui_file = os.path.join("ui", "playblast3_dialog.ui")
this_dir = os.path.dirname(__file__)
ui_file = os.path.join(this_dir, ui_file)

json_config_file = os.path.join(this_dir, 'studio_configurations.json')
yaml_preset_file = os.path.join(this_dir, 'studio_presets.yml')


def get_maya_main_win():
    # boilerplate for Maya Main Window
    try:
        from shiboken import wrapInstance
    except ImportError:
        from PySide.shiboken import wrapInstance

    main_window_ptr = OmUI.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtGui.QWidget)


class ConfigReader():
    """
    Class for reading json configurations for UI configuration

    # QCombobox uses json list for menu choices
    # Nested list for [Label, [value, value]]
    # QLineEdit uses json list of ["mayaFlag", value, "Label"]
    # QCheckbox uses json dict of {"mayaFlag", [bool, "Label"]}

    """

    def __init__(self, config_file=None):
        if not config_file:
            config_file = json_config_file

        try:

            with open(config_file, 'r') as json_reader:
                self.configs = json.load(json_reader)

        except IOError as e:
            logger.debug("Error: {0}".format(e))
            self.configs = None

    @property
    def configurations(self):
        return self.configs


class PresetReader():
    """
    YAML Studio Preset Reader

    """
    _presets = None

    def __init__(self, preset_file=None):

        if not preset_file:
            preset_file = yaml_preset_file

        try:
            with open(preset_file, 'r') as yaml_reader:
                self._presets = yaml.load(yaml_reader)

        except IOError as e:
            logger.debug("Error: {0}".format(e))

    @property
    def presets(self):
        return self._presets


class PlayblastTool3GUI(QtGui.QWidget):
    def __init__(self, app, handler, parent=None):
        """
        class init method. Set parent to Maya Main Window if no parent is given
        :param parent:
        :return:
        """
        #
        # Shotgun Toolkit init code
        #

        self._app = app
        self._handler = handler

        #
        # Main Code
        #

        super(PlayblastTool3GUI, self).__init__(parent=parent)
        pys.loadUi(ui_file, self)

        # set HUD to false
        self.hud_state = False

        config_reader = ConfigReader()
        self.configs = config_reader.configurations

        preset_reader = PresetReader()
        self.presets = preset_reader.presets

        self.format_menu = QtGui.QComboBox()
        self.resolution_menu = QtGui.QComboBox()
        self.compression_stack = QtGui.QStackedWidget()


        # Build UI from configurations
        format_idx = 0
        self.format_dict = {}
        for item in self.configs['format']:
            compression_menu = QtGui.QComboBox()
            compression_menu.setObjectName(item)
            self.format_dict[item] = {"index": format_idx, "widget": compression_menu}
            format_idx += 1

        self.filename_generated = False

        self.custom_wh_keys = ("__width", "__height")
        self.custom_wh_dict = {}

        self.special_line_edit_dict = {}
        self.special_checker_dict = {}

        self.line_edit_array = []
        self.checker_array = []

        self.build_format_menu()
        self.build_resolution_menu()
        self.build_option_forms()
        self.build_checkers()
        self.build_compression_menus()

        self.format_menu.activated.connect(self.rebuild_compression_menu)

        # Main action btn
        self.doPlayblast.clicked.connect(self.do_playblast)

        # Checkbox
        self.checkUploadMovieToSG.clicked.connect(self.toggle_upload_to_sg)
        self.checkUseCustomHUD.clicked.connect(self.toggle_use_custom_hud)

        # Set icon path
        self.icon_path = os.path.join(os.path.dirname(__file__), "icons")

        # Help Menu
        help_btn = QtGui.QPushButton(QtGui.QIcon("{0}\\question2.png".format(self.icon_path)),
                                     u'Launch Web Help')
        help_btn.setMaximumWidth(140)
        help_btn.clicked.connect(self.show_web_help)

        # Extra Run Playblast
        exec_btn = QtGui.QPushButton(QtGui.QIcon("{0}\\cloud122.png".format(self.icon_path)),
                                     u'Run Playblast')
        exec_btn.setMaximumWidth(120)
        exec_btn.clicked.connect(self.do_playblast)

        # Load default HUD
        hud_btn = QtGui.QPushButton(QtGui.QIcon("{0}\\circles1.png".format(self.icon_path)),
                                    u'Toggle Custom HUD')
        hud_btn.setMaximumWidth(160)
        hud_btn.clicked.connect(self.toggle_hud)

        self.topBtnBar.addWidget(help_btn)
        self.topBtnBar.addWidget(exec_btn)
        self.topBtnBar.addWidget(hud_btn)

        self.move(125, 125)

    def toggle_hud(self):
        if self.hud_state:
            hud.cleanup()
            self.hud_state = False
        else:
            hud.run()
            self.hud_state = True

    def toggle_upload_to_sg(self):
        self._app.log_info("Currently Upload to Shotgun is {}".format(self._handler.get_upload_to_shotgun()))
        if self._handler.get_upload_to_shotgun():
            self._handler.set_upload_to_shotgun(False)
        else:
            self._handler.set_upload_to_shotgun(True)

    def toggle_use_custom_hud(self):
        self._app.log_info("Currently Use Custom HUD is {}".format(self._handler.get_use_custom_hud()))

        if self._handler.get_use_custom_hud():
            self._handler.set_use_custom_hud(False)
        else:
            self._handler.set_use_custom_hud(True)

    def build_preset_button_menus(self):

        if not isinstance(self.presets, dict):
            print "Preset: << {0} >> not valid Dictionary".format(yaml_preset_file)
            return

        action_menus = []

        for key in sorted(self.presets['Camera Selection Expression'].keys()):
            icon = QtGui.QIcon("{0}\\circles1.png".format(self.icon_path))
            action = QtGui.QAction(icon, key, self)
            this_partial = partial(self.load_regex, key)
            action.triggered.connect(this_partial)
            action_menus.append(action)

        self.preset_menu.addActions(action_menus)

    def load_regex(self, key=None):
        if key:
            try:
                self.camSelectExpression.setText(self.presets['Camera Selection Expression'][key])
            except ValueError as e:
                print e

            self.statusBar().showMessage("Loading Preset: {0}".format(key), 1500)

    @staticmethod
    def show_web_help():
        pm.launch(webPage="https://gitlab.com/bigger-picture-develop-group" +
                          "/bigger-picture-pipeline-documentation/wikis/Batch_Playblast_Doc")

    @property
    def default_resolution(self):
        try:
            return self.configs["resolution"][0].items()[0]
        except ValueError as e:
            print "Error getting default resolution: {0}".format(e)
            return [960, 540]

    def build_format_menu(self):
        """
        Build menu item from json configuration file "Format"
        :return:
        """
        self.format_menu.addItems(self.configs['format'])
        self.OptionLayout1.addRow('Format', self.format_menu)

    def build_resolution_menu(self):
        """
        Build menu item from json configuration file "Resolution"
        :return:
        """
        for item in self.configs['resolution']:
            self.resolution_menu.addItems(item.keys())
        self.OptionLayout1.addRow("Resolution", self.resolution_menu)

    def build_compression_menus(self):
        """
        Build menu item from json configuration file "Compression", store result menu widget in QStack widget
        :return:
        """
        for key in self.format_dict.keys():
            # Add item list to the QComboBox by format key (qt, image, etc.)
            self.format_dict[key]["widget"].addItems(self.configs["compression"][key])
            # Insert QComboBox widget into QStack widget by format key
            self.compression_stack.insertWidget(self.format_dict[key]["index"], self.format_dict[key]["widget"])

        self.compression_stack.setCurrentIndex(0)

        self.OptionLayout1.addRow('Compression', self.compression_stack)

    def rebuild_compression_menu(self):
        """
        Rebuild compression depends on format menu choice
        :return:
        """
        current_format = str(self.format_menu.currentText())
        idx = self.format_dict[current_format]["index"]
        self.compression_stack.setCurrentIndex(idx)

    def build_option_forms(self):
        """
        Build options line edit widgets
        :return:
        """
        for item in self.configs["options"]:
            line_edit = QtGui.QLineEdit()
            line_edit.setText(str(item[1]))
            line_edit.setObjectName(item[0])
            self.OptionLayout3.addRow(item[2], line_edit)

            self.line_edit_array.append(line_edit)

            for key in self.custom_wh_keys:
                if line_edit.objectName() == key:
                    self.custom_wh_dict[key] = line_edit

            if "__" in line_edit.objectName():
                self.special_line_edit_dict[line_edit.objectName()] = line_edit

    def build_checkers(self):
        """
        Build check boxes for bool flag
        :return:
        """
        for item in self.configs["checkers"].items():
            checker = QtGui.QCheckBox()
            checker.setChecked(bool(item[1][0]))
            checker.setObjectName(item[0])
            self.OptionLayout2.addRow(item[1][1], checker)

            if "__" in checker.objectName():
                self.special_checker_dict[checker.objectName()] = checker

            self.checker_array.append(checker)

    def do_widget_array_test(self):
        for item in self.checker_array:
            print "<< {0} >> widget hold value: {1}".format(item.objectName(), bool(item.checkState()))
        for item in self.line_edit_array:
            print "<< {0} >> widget hold value: {1}".format(item.objectName(), item.text())

    @staticmethod
    def get_camera_transform(cam_name):

        cam_name = pm.PyNode(cam_name)
        if cam_name.type() == 'camera' or cam_name.type() == 'stereoRigCamera':
            cam_name = cam_name.listRelatives(parent=True, type='transform')[0]

        return cam_name

    def setup_use_sound(self, job_flag_dict):
        """
        setup job flag options for sound file
        :param job_flag_dict:
        :return:
        """
        # If check useSound, get sound node from time slider then create flag for sound
        if bool(self.special_checker_dict["__useSound"].checkState()):
            playback_slider = mel.eval('$tmpVar=$gPlayBackSlider')
            sound_node = pm.timeControl(playback_slider, q=True, sound=1)
            if isinstance(sound_node, list):
                sound_node = sound_node[0]
            if isinstance(sound_node, pm.PyNode):
                sound_node = sound_node.name()
            job_flag_dict['sound'] = str(sound_node)

        return job_flag_dict

    def setup_use_start_end(self, job_flag_dict):
        """
        setup custom start to end frames for single batch mode
        :param job_flag_dict:
        :return:
        """
        # If check useStartEnd, duration is change to use custom start and end time
        if bool(self.special_checker_dict["__useStartEnd"].checkState()):
            job_flag_dict["startTime"] = self.special_line_edit_dict["__startTime"].text()
            job_flag_dict["endTime"] = self.special_line_edit_dict["__endTime"].text()

        return job_flag_dict

    def setup_stereo_view(self, job_flag_dict):
        """
        setup job flag option for stereo view
        :param job_flag_dict:
        :return:
        """
        # if check stereo view, use cameraSetup flag to setup stereo view before playblast, do one playblast
        # for each eye
        if bool(self.special_checker_dict["__stereoCamOutput"].checkState()):

            stereo_view_list = ("left", "right")
            stereo_job_list = []
            for eye in stereo_view_list:
                stereo_job_list.append(
                    ("stereoCameraView -e -displayMode {eye}Eye StereoPanelEditor".format(eye=eye),
                     "{eye}".format(eye=eye))
                )
            job_flag_dict["cameraSetup"] = stereo_job_list

        return job_flag_dict

    @property
    def current_camera_transform(self):
        """
        python property to get current camera transform
        :return:
        """
        cam = self.current_camera
        return cam.listRelatives(parent=True, type='transform')[0]

    @property
    def current_camera(self):
        """
        python property to get current camera
        :return:
        """
        active_e = pm.playblast(activeEditor=True)
        the_cam = pm.modelEditor(active_e, q=True, camera=True)

        print "Current Cam is: {0}\n".format(the_cam)

        if type(the_cam) == "transform":
            return the_cam.getShape()
        else:
            return the_cam

    def get_regex_compiled(self):
        """
        compile RegEx from UI selection expression QLineEdit
        :return:
        """
        inpt_str = str(self.camSelectExpression.text())
        return return_regex_compiled(inpt_str)

    def get_maya_filtered_cameras(self):
        """
        Use RegEx to filter camera selections
        :return:
        """
        filtered_list = pm.ls(regex=str(self.camSelectExpression.text()))
        filtered_list = [item for item in filtered_list if item.type() == 'camera' or item.type() == 'stereoRigCamera']
        filtered_list = [item.listRelatives(parent=1, type='transform')[0] for item in filtered_list]

        return filtered_list

    def get_resolution(self):
        """
        Get current resolution setting from the QCombo menu
        If user choose custom get the custom value from special custom width height QLineEdit
        :return:
        """
        # Get the width height from the json config if key match the menu choice
        cur_res = str(self.resolution_menu.currentText())  # current rez choice

        try:
            cur_wh = [item[cur_res] for item in self.configs['resolution'] if item.keys()[0] == cur_res][0]
        except ValueError as e:
            print "Error getting resolution from configs: {0}".format(e)
            cur_wh = self.default_resolution

        # If current res choice is "Custom", then get the line edit widgets where the current values hold
        # the key for width height is _width and _height to avoid clash with QT keyword
        if cur_wh == "Custom":
            try:
                cur_wh = [int(self.custom_wh_dict[self.custom_wh_keys[0]].text()),
                          int(self.custom_wh_dict[self.custom_wh_keys[1]].text())]
            except StandardError as e:
                print "Error: {0}".format(e)
                print "Cannot convert resolution"
                return self.default_resolution

        return cur_wh

    def setup_resolution(self, job_flag_dict):
        cur_wh = self.get_resolution()
        job_flag_dict['widthHeight'] = cur_wh
        print "Resolution: {0}\n".format(str(self.resolution_menu.currentText()))

        return job_flag_dict

    def setup_common_job_dict(self):
        """
        Setup common job dict for both playblast mode (single and batch), dict is used to contract flag option for
        Maya cmds.playblast() function
        :return: job_flag_dict
        """
        job_flag_dict = {}

        cur_format = str(self.format_menu.currentText())

        print "\nFormat: {0}".format(cur_format)
        print "Compression: {0}\n".format(str(self.compression_stack.currentWidget().currentText()))

        job_flag_dict['format'] = cur_format
        job_flag_dict['compression'] = str(self.compression_stack.currentWidget().currentText())

        return job_flag_dict

    def setup_general_options(self, job_flag_dict):
        """
        setup general options from list of QLineEdit and QCheckbox
        :param job_flag_dict:
        :return:
        """
        # Skip any key that has "__" because these are special are needed to deal separately
        for item in self.line_edit_array:
            if "__" not in item.objectName():
                job_flag_dict[str(item.objectName())] = int(item.text())

        for item in self.checker_array:
            if "__" not in item.objectName():
                job_flag_dict[str(item.objectName())] = bool(item.checkState())

        return job_flag_dict

    def setup_default_job_flags(self, job_flag_dict):

        defaults = self.configs.get('defaults', None)

        self._app.log_info("Default Values are: \n{}\n".format(pformat(defaults)))

        if isinstance(defaults, dict):
            for key, val in defaults.items():
                job_flag_dict[str(key)] = val

        return job_flag_dict

    def setup_hud(self):
        """
        Run previz_hud before playblast
        :return:
        """
        # check to see if we need stereo hud, else start regular hud
        if bool(self.checkUseCustomHUD.checkState()):
            try:
                if bool(self.special_checker_dict["__stereoCamOutput"].checkState()):
                    hud.run(config='stereo playblast')
                else:
                    hud.run(config='playblast compact')
            except StandardError as e:
                print "Error: Cannot get check state".format(e)
                hud.run(config='playblast')

    def validate_stereo_cam(self, cam=None):

        if not cam:
            cam = self.current_camera

        cam = pm.PyNode(cam)

        if cam.type() == 'stereoRigCamera':
            print "This {0} is stereo cam\n".format(cam)
            return True
        else:
            return False

    def validate_stereo_camera_job(self, job_flag_dict, cam_name=None):
        if not cam_name:
            cam_name = self.current_camera

        # if we got stereo option turn on but the camera is not a stereo cam pop the camera setup flag
        if 'cameraSetup' in job_flag_dict:
            if not self.validate_stereo_cam(cam_name):
                job_flag_dict.pop('cameraSetup', None)

        return job_flag_dict

    def do_playblast(self):
        """
        get Job flags from UI then call handler's class do_playblast method with job flag data
        :return:
        """
        job_flag_dict = self._make_job_flag_dict()

        if not job_flag_dict:
            pm.confirmBox(title="Job Flag Not Valid!",
                          message="Cannot construct correct job flags")
            return

        self._handler.do_playblast(job_flag_dict)

    def _make_job_flag_dict(self):

        """
        Construct job flag dict for pm.playblast() function later
        :return:
        """

        job_flag_dict = self.setup_common_job_dict()
        job_flag_dict = self.setup_default_job_flags(job_flag_dict)

        # get active camera name
        cam_name = self.current_camera

        job_flag_dict = self.setup_use_sound(job_flag_dict)
        job_flag_dict = self.setup_use_start_end(job_flag_dict)
        job_flag_dict = self.setup_stereo_view(job_flag_dict)
        job_flag_dict = self.setup_resolution(job_flag_dict)
        job_flag_dict = self.setup_general_options(job_flag_dict)
        job_flag_dict = self.validate_stereo_camera_job(job_flag_dict, cam_name)

        # if width height cannot be found, playblast should fail
        for item in job_flag_dict["widthHeight"]:
            if not isinstance(item, int):
                print "Width, Height not integer, exit!"
                return

        return job_flag_dict


def return_regex_compiled(input_str=None):
    if not input_str or not isinstance(input_str, str):
        return None

    import re

    return re.compile(input_str)
