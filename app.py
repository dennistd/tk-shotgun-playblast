# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
An app that wrap most of the Maya playblast function into a GUI

"""
import tank
import traceback


class BaseBpPlayblast3(tank.platform.Application):
    bpPlayblastManager = None

    def init_app(self):
        """
        Called as the application is being initialized
        """
        self.engine.register_command("BP Playblast3", self.run_app)

    def destroy_app(self):
        """
        App teardown
        """
        self.log_debug("Destroying bp-playblast3 app")

    def run_app(self):
        """
        Start doing playblast
        """
        try:
            playblast_manager = self.get_playblast_manager()
            playblast_manager.show_ui()
        except StandardError:
            traceback.print_exc()

    def get_playblast_manager(self):
        """
        Create a singleton PlayblastTool3 object to be used by any app.
        """
        if self.bpPlayblastManager is None:
            tk_maya_bp_playblast3 = self.import_module("tk_maya_bp_playblast3")
            self.bpPlayblastManager = tk_maya_bp_playblast3.PlayblastTool3(self)

        return self.bpPlayblastManager
