import os
import re

import maya.cmds as cmds
import pymel.core as pm
import traceback
from contextlib import contextmanager
import logging

FORMAT = '%(asctime)-15s::  %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)

import tank
from tank import Hook


class PrePlayblast(Hook):
    """
    Hook called when creating playblast

    Main hook entry point

    :action:        String
                    setup_hud       => setup HUD using in-house previz_hud system
                    set_output_path => set output path for local playblast output

    """

    _do_custom_hud = True

    def execute(self, action='', data=None, **kwargs):
        # import in house custom hud library
        import previz_hud as hud

        self._do_custom_hud = kwargs.get('do_custom_hud', True)

        # Setup HUD
        if action == "setup_hud":

            logger.info("Executing Custom HUD Setup Hook")

            current_huds = [item for item in pm.headsUpDisplay(listHeadsUpDisplays=True)
                            if pm.headsUpDisplay(item, query=True, visible=True)]

            # if not doing custom huds just return current huds
            if not self._do_custom_hud:
                return current_huds

            # hide all visible HUDs
            map(lambda hud: pm.headsUpDisplay(hud, edit=True, visible=False), current_huds)

            # Turn on in house custom hud
            hud.run()

            return current_huds

        elif action == "set_output_path":

            output_path = kwargs.get('output_path', None)

            if not output_path:
                return None

            if not isinstance(data, dict):
                return None
            data['filename'] = output_path

            return data

        elif action == "restore_hud":

            if not isinstance(data, list):
                return

            # if we got a list of previously used HUD, we restore them

            for hud in data:
                try:
                    pm.headsUpDisplay(hud, edit=True, visible=True)
                except StandardError as e:
                    print "{} HUD item cannot be restored".format(hud)
                    print e

    @property
    def current_camera_transform(self):
        """
        python property to get current camera transform
        :return:
        """
        cam = self.current_camera
        return cam.listRelatives(parent=True, type='transform')[0]

    @property
    def current_camera(self):
        """
        python property to get current camera
        :return:
        """
        active_e = pm.playblast(activeEditor=True)
        the_cam = pm.modelEditor(active_e, q=True, camera=True)

        print "Current Cam is: {0}\n".format(the_cam)

        if type(the_cam) == "transform":
            return the_cam.getShape()
        else:
            return the_cam

    def get_maya_filtered_cameras(self):
        """
        Use RegEx to filter camera selections
        :return:
        """
        filtered_list = pm.ls(regex=str(self.camSelectExpression.text()))
        filtered_list = [item for item in filtered_list if item.type() == 'camera' or item.type() == 'stereoRigCamera']
        filtered_list = [item.listRelatives(parent=1, type='transform')[0] for item in filtered_list]

        return filtered_list
